// Parameters file for Wang Landau if sigma & boundaries are variable

crit_min        = 1e-4
crit_score_flat = 0.6
ln_f            = 1.0
pow_f           = 0.5

sigma_var       = 1
sigma_min       = 0.1
sigma_max       = 2.0

bound_var       = 1
bin_size        = 0.01
Qmin_th         = 0
Qmax_th         = 1

