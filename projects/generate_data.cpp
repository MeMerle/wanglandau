#include"wang_landau.hpp"
#include"func.hpp"

using namespace std;

int main(int argc, char *argv[]){
        
        FcnMap fcnMap = create_FcnMap();
        
        if(argc!=4){ cout << "Number of arguments incorrect! Use as generate_data.cpp wang_landau_parameters_file function_analysis_parameters_file prefix_of_record_files" << endl; return 0;}
        
        const char * wl_params = argv[1];
        const char * fcn_params = argv[2];
        string title = argv[3];
        
        double error_double = 1.23456;
        int error_int = 123456;
        
        GetPot params(fcn_params);         
        int ndim = params("n_dim", error_int);
        int np = params("n_par", error_int);
        const string fcn_str = params("FcnType", "error");
        ptrFcn fcn = fcnMap[fcn_str]; 
        if(ndim == error_int || np == error_int || fcn_str == "error"){ cout << "Error in function definition ! Check function name, # of dimensions and # of function parameters in the entry file. " << endl; return 0;}
        
        srand(time(NULL));
        
        double min_v = params("var_boundaries", error_double, 0);
        double max_v = params("var_boundaries", error_double, 1);
        if(min_v == error_double || max_v == error_double){ cout << "Error, explored space is not limited ! Check the function parameters file please." << endl; return 0; }
        if(min_v >= max_v){ cout << "Error, min_var >= max_var ! Check the function parameters file please." << endl; return 0; }
        double *low_bnd = (double*)malloc(ndim*sizeof(double));
        double *upp_bnd = (double*)malloc(ndim*sizeof(double));
        for(int i=0; i<ndim; i++){
                low_bnd[i] = min_v;
                upp_bnd[i] = max_v;
        }
        
        cout << endl << endl << "Function : " << fcn_str <<  " in " << ndim << " dimension(s). " << endl; 
        cout << "Explored space: " << ndim << "d-hypercube between " << min_v << " and " << max_v << endl;
        
        double param_def = params("param_def", 0);
        double *param_fcn = (double*)malloc(np*sizeof(double));
        
        if(param_def){
                for(int i=0; i<np; i++){
                        param_fcn[i] = params("param_fcn",error_double,i);
                        if(param_fcn[i] == error_double){cout << "Error function parameters are not furnished, desactivate param_def or check the entry file!" << endl; return 0;}
                }
        }
        else{
                double *min_p;
                double *max_p;
                double  p =params("par_boundaries",error_double, 2);
                //                 cout << "check 1 " << endl;
                //                 cout << "p " << p << endl;
                if(p==error_double){
                        min_p=(double*)malloc(1*sizeof(double));
                        max_p=(double*)malloc(1*sizeof(double));
                        min_p[0] = params("par_boundaries", error_double, 0);
                        max_p[0] = params("par_boundaries", error_double, 1);
                        if(min_p[0] == error_double || max_p[0] == error_double){ cout << "Error, function parameters are not furnished and not limited ! Check the entry file please " << endl; return 0; } 
                        for(int i=0; i<np; i++){
                                param_fcn[i] = min_p[0] + (max_p[0] - min_p[0])*drand();
                        }
                        //                         cout << "check 2a " << endl;
                        
                }
                else{
                        //                         cout << "check 2b " << endl;
                        min_p=(double*)malloc(np*sizeof(double));
                        max_p=(double*)malloc(np*sizeof(double));
                        for(int i=0; i<np; i++){
                                min_p[i] = params("par_boundaries",error_double, 2*i);
                                max_p[i] = params("par_boundaries",error_double, 2*i+1);
                                param_fcn[i] = min_p[i] + (max_p[i] - min_p[i])*drand();
                                if(min_p[i] ==error_double || max_p[i] == error_double){ cout << "Error, function parameters are not furnished and not limited ! Check the entry file please " << endl; return 0; } 
                        }
                        free(min_p);
                        free(max_p);
                }
        }
        cout << "Function parameters : (" ;
        for(int i=0; i<np; i++){
                if(i!=np-1) cout << param_fcn[i] << ", " ;
                else cout << param_fcn[i] << ")" << endl;
        }
        
        
        
        int if_wl = params("if_wl", 0);
        int if_random = params("if_random", 0);
        int if_detailed = params("if_detailed",0);
        if(if_wl + if_random + if_detailed ==0) { cout << " Nothing asked ! (No Wang Landau, no random, no detailed) - Check parameters file!" << endl; return 0;  }
        
        ifstream file;
        ofstream summary;
        string ss ;
        summary.open(title + ".summary");
        file.open(fcn_params);
        while(getline(file, ss)){
                summary << ss.c_str() << endl;
        }
        summary << endl;
        
        summary << "// Actual parameters of the function : " << endl ;
        summary << "Params\t=\t";
        for(int i=0; i<np-1;i++){
                summary << param_fcn[i] << "  " ;
        }
        summary << param_fcn[np-1] << endl<< endl << endl; 
        
        Wang_Landau WL;                
        if(if_wl){
                WL.wang_landau(fcn, ndim, np, param_fcn, low_bnd, upp_bnd, wl_params, title);
                

                file.open(wl_params);
                summary.open(title  + ".summary");
                while(getline(file, ss)){
                        summary << ss.c_str() << endl;
                }
                summary << endl;
                file.close();
                
                
                summary << "// Final parameters of the Wang Landau" << endl << endl;
                summary.close();
                WL.Save_all_parameters(title + ".summary");
                
                cout << "WL over. Generating an equal number of random points." << endl;
        }
        
        
        if(if_random){
                int nrand=0;
                if(if_wl) nrand = WL.n_walk_tot;
                else nrand = params("nrand", pow(10,ndim));
                
                clock_t t = clock();
                gener_random(fcn, ndim, np, param_fcn, nrand, low_bnd, upp_bnd, title);
                t= clock()-t;
                cout << nrand << " random points generated in " << double(t) /((double)CLOCKS_PER_SEC* 60.0) << " min." << endl << endl;
        }
        
        
        //         if(if_detailed){
        //                 double precision = params("precision", 0.1);
        //                 clock_t t = clock();
        //                 int ntot = gener_details(fcn, ndim, param_fcn, low_bnd, upp_bnd, precision);
        //                 t= clock()-t;
        //                 cout << ntot << " points computed to get a detailed vision of the function with a precision " <<  precision << " in " << double(t) /((double)CLOCKS_PER_SEC* 60.0) << " min." << endl << endl;
        //                 
        // //                 int ntot = 1;
        // //                 double * x_tot=(double*)malloc(ntot*sizeof(double));
        // //                 x_tot[0] = 0;
        // //                 for(int i=0; i<20; i++){
        // //                                 ntot ++;
        // //                                 cout << "ntot " << ntot  << "  xtot " << x_tot << endl;
        // //                                 realloc_s((void**)&x_tot, ntot*sizeof(double));
        // //                                 cout << "ntot " << ntot  << "  xtot " << x_tot << endl;
        // //                                 x_tot[i+1] = i+1;
        // //                 }
        // //                 free(x_tot);
        //                 
        //                 
        //         }
        
        free(param_fcn);
        free(low_bnd);
        free(upp_bnd);
        
        return 0;
        
}
