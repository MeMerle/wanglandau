#!/bin/bash

nd=$1
original_folder=$2
destination_folder=$3
prefix=gauss_$nd
prefix+=_n

N=1
for X in $original_folder/WL_gaussfun_$nd*.walk; do
  file=$prefix
  file+=$N
  file+="_A.txt"
  cp $X $destination_folder/$file
  N=$(($N+1))
done

N=1
for X in $original_folder/WL_gaussfun_$nd*.random; do
  file=$prefix
  file+=$N
  file+="_B.txt"
  cp $X $destination_folder/$file
  N=$(($N+1))
done

N=1
for X in $original_folder/WL_gaussfun_$nd*.sigma; do
  file=$prefix
  file+=$N
  file+="_sigma.txt"
  cp $X $destination_folder/$file
  N=$(($N+1))
done
