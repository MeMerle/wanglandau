#include"wang_landau.hpp"

using namespace std;

Wang_Landau::Wang_Landau(const char * param_file){
        
        Init(param_file);
}


Wang_Landau::~Wang_Landau(){
        free(histogram);
        free(log_density);
}

int Wang_Landau::Init(const char * param_file){
        
        int err=0;
        t_0=clock();
        n_iteration=0;
        
        double error_double = 1.23456;
        int error_int = 123456;
        
        GetPot params(param_file); 
        crit_min=params("crit_min", error_double);
        crit_score_flat=params("crit_score_flat", error_double);
        lnf = params("ln_f",error_double);
        f=exp(lnf);
        pow_f=params("pow_f",error_double);
        
        n_update = params("n_update",50000);
        
        if(crit_min == error_double || crit_score_flat == error_double || lnf == error_double || pow_f == error_double) {
                cout << "Error, Wang Landau parameters missing!" << endl;
                err=1;
        }
        
        sigma_var=params("sigma_var", false);
        if(sigma_var){
                sigma_min=params("sigma_min",error_double);
                sigma_max=params("sigma_max",error_double);
                sigma=(sigma_min+sigma_max)/2.0;
        }
        else{
                sigma=params("sigma",error_double);
                sigma_min=sigma; 
                sigma_max=sigma;
        }
        if(sigma_min == error_double || sigma_max == error_double || sigma == error_double){
                cout << "Error, sigma description not provided!" << endl;
                err=1;
        }
        
        bound_var=params("bound_var",false);
        if(bound_var){
                bin_size=params("bin_size", error_double);
                Qmin_th=params("Qmin_th",error_double);
                Qmax_th=params("Qmax_th",error_double);
                if(Qmin_th == error_double) Qmin_th = numeric_limits<double>::min();;
                if(Qmax_th == error_double) Qmax_th = numeric_limits<double>::max();;
        }
        else{               
                Qmin=params("Qmin", error_double);
                Qmax=params("Qmax", error_double);
                nbins=params("nbins",error_int);
                bin_size=params("bin_size",error_double);
                if(nbins == error_int) nbins = (Qmax-Qmin)/bin_size;
        }
        if(nbins == error_int && bin_size == error_double){
                cout << "Error, histogram description not provided !" << endl;
                err=1;
        }

        n_walk_tot=0;
        bound_modified=false;
        
        
        return err;
        
}

int Wang_Landau::Restart(const char * param_file){
        
        int err=0;
        
        double error_double = 1.23456;
        int error_int = 123456;
        
        GetPot params(param_file); 
        crit_min=params("crit_min", error_double);
        crit_score_flat=params("crit_score_flat", error_double);
        lnf = params("ln_f",error_double);
        f=exp(lnf);
        pow_f=params("pow_f",error_double);
        
        if(crit_min == error_double || crit_score_flat == error_double || lnf == error_double || pow_f == error_double) {
                cout << "Error, Wang Landau parameters missing!" << endl;
                err=1;
        }
        
        sigma_var=params("sigma_var", false);
        if(sigma_var){
                sigma_min=params("sigma_min",error_double);
                sigma_max=params("sigma_max",error_double);
                sigma=(sigma_min+sigma_max)/2.0;
        }
        else{
                sigma=params("sigma",error_double);
                sigma_min=sigma; 
                sigma_max=sigma;
        }
        if(sigma_min == error_double || sigma_max == error_double || sigma == error_double){
                cout << "Error, sigma description not provided!" << endl;
                err=1;
        }
        
        bound_var=params("bound_var",false);
        if(bound_var){
                bin_size=params("bin_size", error_double);
//                 Qmin_th=params("Qmin_th",error_double);
//                 Qmax_th=params("Qmax_th",error_double);
                if(Qmin_th == error_double) Qmin_th = numeric_limits<double>::min();;
                if(Qmax_th == error_double) Qmax_th = numeric_limits<double>::max();;
        }
        else{               
                Qmin=params("Qmin", error_double);
                Qmax=params("Qmax", error_double);
                nbins=params("nbins",error_int);
                bin_size=params("bin_size",error_double);
                if(nbins == error_int) nbins = (Qmax-Qmin)/bin_size;
        }
        if(nbins == error_int && bin_size == error_double){
                cout << "Error, histogram description not provided !" << endl;
                err=1;
        }
        
        bound_modified=false;
        Reset_logd();
        n_loop=1;
        n_iteration++;
        n_walk_loop=0;
        n_walk_restart=0;
        
        return err;
                
}

void Wang_Landau::Init(double Q){
        if(bound_var){
                Qmin=ceil( (Q-2*bin_size)/bin_size )*bin_size; 
                Qmax=floor( (Q+2*bin_size)/bin_size )*bin_size; 
                if(Qmin<Qmin_th) Qmin=Qmin_th;
                if(Qmax>Qmax_th) Qmax=Qmax_th;
                score =0;
                nbins=(Qmax-Qmin)/bin_size;
        }
        log_density = (double*)calloc(nbins, sizeof(double));          
        histogram = (int*)calloc(nbins, sizeof(int));
        
        t_loop=clock();
        
        n_loop=0;
        
        bin_old = floor((Q-Qmin)*nbins/(Qmax-Qmin));
        histogram[bin_old]++;
        log_density[bin_old]+=lnf;  
        n_walk_loop=1;
        n_walk_restart=1;
        n_walk_tot++;        
}




void Wang_Landau::Update_score(){
        score= 0;
        double min = histogram[0];
        for(int i = 1 ; i < nbins; i++){
                if( histogram[i] < min){
                        min = histogram[i];
                }
        }
        score = min/n_walk_loop*nbins;
};

void Wang_Landau::Update_sigma(){
        double meanlog_dens=0;
        for(int i=0; i<nbins; i++){
                meanlog_dens += log_density[i];
        }
        meanlog_dens/= nbins;        
        sigma = sigma_min + (sigma_max-sigma_min) * pow(log_density[bin_old]/meanlog_dens, 1/(double)ndim) ;        
}

int Wang_Landau::Update_bounds(double Q){
        int nadd=0;
        
        cout << "Updating histogram boundaries: " ;
        
        if( Q < Qmin){
                nadd = ceil( (Qmin-Q)/bin_size);
                nbins += nadd;
                
                realloc_s((void**)&log_density, nbins*sizeof(double));
                realloc_s((void**)&histogram, nbins*sizeof(int));
                
                decal(log_density, nbins-nadd, nadd);                
                decal(histogram, nbins-nadd, nadd);
                
                Qmin -= nadd*bin_size;
                if(Qmin<Qmin_th){
                        Qmin=Qmin_th;
                        cout << "Caution, it seems like one value went out of theoretical boundaries of Q .. " << endl << "Q = " << Q << endl;
                        return 1;
                }
                bin_old+=nadd;
        }
        
        if ( Q > Qmax){
                nadd = ceil((Q-Qmax)/bin_size);
                nbins += nadd;
                
                realloc_s((void**)&log_density, nbins*sizeof(double));
                realloc_s((void**)&histogram, nbins*sizeof(int));
                memset(log_density+nbins-nadd, 0, nadd*sizeof(double));
                memset(histogram+nbins-nadd, 0, nadd*sizeof(int));
                
                Qmax += nadd*bin_size;
                if(Qmin<Qmin_th){
                        Qmin=Qmax_th;
                        cout << "Caution, it seems like one value went out of theoretical boundaries of Q .. " << endl;
                        return 1;
                }
                
        }
        
        cout << "Qmin = " << Qmin << ", Qmax = " << Qmax << ", nbins = " << nbins << endl;
        
        bound_modified=true;        
        return 0;
}

bool Wang_Landau::Step(double Q){
        
        int err =0;
        if(bound_var){
                if(Q< Qmin || Q> Qmax){
                        err = Update_bounds(Q);
                }
        }
        
        bin_new = floor((Q-Qmin)*nbins/(Qmax-Qmin));
        bool accept = false;
        if(err){ return accept; cout << " Value of Q = " << Q << " ignored " << endl;}
        double proba=exp(log_density[bin_old]-log_density[bin_new]);
        if( drand() < proba){
                accept=true;
                bin_old=bin_new;
        }
        else{
                accept=false;
        }
        
        histogram[bin_old]++;
        log_density[bin_old]+=lnf;                
        n_walk_loop++;
        n_walk_restart++;
        n_walk_tot++;
        
        Update_score();
        if(sigma_var) Update_sigma();
        return accept;        
}



void Wang_Landau::Measure_looptime(){
        t_loop = clock() - t_loop;
}

void Wang_Landau::Measure_totaltime(){
        t_tot = clock() - t_0;
}

void Wang_Landau::Reset_hist(){
        memset(histogram, 0, nbins*sizeof(int));
}

void Wang_Landau::Reset_logd(){
        memset(log_density, 0, nbins*sizeof(double));
}

void Wang_Landau::Norm_logd(){
        double min = log_density[0];
        for(int i=0; i< nbins; i++){
                if (log_density[i] < min){
                        min = log_density[i];
                }
        }
        for(int i=0; i<nbins; i++){
                log_density[i] -= min;
        }
}

void Wang_Landau::End_loop(){
        f = pow(f, pow_f);
        lnf = log(f);
        score   = 0;
        Reset_hist();
        Norm_logd();
        n_loop++;
        n_walk_loop=0;
}


void Wang_Landau::Print_density(){
        cout << " Density (log) : " << endl;
        print(log_density, 1,nbins);
}

void Wang_Landau::Print_hist(){
        cout << " Histogram : " << endl;
        print(histogram,1,nbins);
}


int Wang_Landau::Init_save_hist(string filename){
        
        ofstream file; 
        if(fexists(filename)){ cout << endl << "File already existing (.hist) - stop here!" << endl; return 0; }
        file.open(filename);  
        
        file << "restart" << "\t" ; 
        file << "n_loop" << "\t" << "f" << "\t\t";
        file << "Qmin" << "\t" << "Qmax" << "\t" << "nbins" << "\t" << "bin_size" << "\t";
        file << setprecision(5) << "time(min)" << "\t" ;
        file << "npts" << "\t" ;
        for(int i=0; i< nbins; i++){
                file << "bin_" << i << "\t" ;
        }
        file << endl;
        file.close();
        
        return 1;
        
}

void Wang_Landau::Save_hist(string filename){
        
        ofstream file; 
        file.open(filename, ios::app);
        
        file << n_iteration << "\t" ; 
        file << n_loop << "\t" << f << "\t\t";
        file << Qmin << "\t" << Qmax << "\t" << nbins <<  "\t" << bin_size << "\t" ;
        file << setprecision(5) << double(t_loop) / CLOCKS_PER_SEC /60 << "\t\t" ;
        file << n_walk_loop << "\t" ;
        for(int i=0; i< nbins; i++){
                file << log_density[i] << "\t" ;
        }
        file << endl;        
        file.close();
}

int Wang_Landau::Init_save_walk(string filename){
        
        if(fexists(filename)){ cout << endl <<  "File already existing (.walk) - stop here!" << endl; return 0; }        
        return 1;
        
}

void Wang_Landau::Save_walk(string filename, double *x, double Q){
        
        ofstream file; 
        file.open(filename, ios::app);
        for(int i=0; i<ndim; i++){
                file << x[i] << "\t" ;
        }
        file << Q << endl;
        file.close();
}

int Wang_Landau::Init_update(string filename){
        
        if(fexists(filename)){ cout << endl <<  "File already existing (.update) - stop here!" << endl; return 0; }
        return 1;
        
}

void Wang_Landau::Update(string filename){
        
        ofstream update;
        update.open(filename);
        
        update << "(" << n_iteration << "," << n_loop << "," << n_walk_loop << ")" << ", average = " << n_walk_loop/nbins << ", score = " << score << ", Qmin = " << Qmin << ", Qmax = " << Qmax  << ", sigma min= " << sigma_min << ",  sigma max= "    << sigma_max << ", sigma= " << sigma << endl;
        
        update << "Log density : " << endl;
        for(int i=0; i<nbins; i++){
                update << log_density[i] << "\t" ;
        }
        update << endl;
        
        update << "Histogram : " << endl;
        for(int i=0; i<nbins; i++){
                update << histogram[i] << "\t" ;
        }
        update << endl;
        
        update.close();        
}

void  Wang_Landau::Save_all_parameters(string filename){
        
        ofstream file;
        file.open(filename, ios::app);
        
        // parameters of the Wang Landau
        file << "crit_min\t = " << crit_min << endl;;
        file << "crit_score_flat\t = " << crit_score_flat << endl;;
        file << "lnf\t = " << lnf << endl;;
        file << "f\t = " << f << endl;;
        file << "pow_f\t = " << pow_f << endl;;
        file << "crit_min\t = " << crit_min << endl;;
        
        // parameters for variables update
        file << "ndim\t = " << ndim << endl;;
        file << "sigma\t = " << sigma << endl;;
        file << "sigma_min\t = " << sigma_min << endl;;
        file << "sigma_max\t = " << sigma_max << endl;;
        file << "sigma_var\t = " << sigma_var << endl;;
        
        // parameters of the Q histogram
        file << "bound_var\t = " << bound_var << endl;;
        file << "bound_modified\t = " << bound_modified << endl;;
        file << "Qmin\t = " << Qmin << endl;;
        file << "Qmax\t = " << Qmax << endl;;
        file << "Qmin_th\t = " << Qmin_th << endl;;
        file << "Qmax_th\t = " << Qmax_th << endl;;
        file << "nbins\t = " << nbins << endl;;
        file << "bin_size\t = " << bin_size << endl;;
        
        // variables updated during the algorithm
        file << "Log density : " << endl;
        for(int i=0; i<nbins; i++){
                file << setprecision(5) << log_density[i] << "\t\t" ;
        }
        file << endl;
        
        // counting 
        file << "n_iteration\t = " << n_iteration << endl;;
        file << "n_loop\t = " << n_loop << endl;;
        file << "n_walk_loop\t = " << n_walk_loop << endl;;
        file << "n_walk_restart\t = " << n_walk_restart << endl;;
        file << "n_walk_tot\t = " << n_walk_tot << endl;;
        file << "n_update\t = " << n_update << endl;;

        // counting time        
        file << "t_tot\t = " << double(t_tot) /((double)CLOCKS_PER_SEC* 60.0) << "min"  << endl;
        
}



int Wang_Landau::wang_landau(ptrFcn fcn, int n_dim, int np, double *param_fcn, double *low_bnd, double *upp_bnd, const char *param_file, string record_file){
        
        cout << endl << "Wang Landau initialization... " << endl;
        
        int err=Init(param_file);
        if(err) return 0;
        ndim = n_dim;
        cout << "Parameters filled." << endl;
        
        mt19937_64 generator =initiate_random_generator();
        normal_distribution<double> normal; // default is 0 mean and 1.0 std;
        cout << "Random gaussian generator created." << endl;
        
        double *x = (double*)calloc(ndim, sizeof(double));
        double *xtest = (double*)calloc(ndim, sizeof(double));
        
        for(int i=0; i<ndim;i++){
                x[i] = low_bnd[i] + drand()*(upp_bnd[i] - low_bnd[i]);
        }
        double Q = fcn(ndim, x, np, param_fcn);
        cout << "Going to initalize with first Q value, Q = "<< Q  << endl;
        Init(Q);
        cout << "First Q value placed in the histogram. " <<endl;

        string hist_str, walk_str , update_str; 
        hist_str = record_file + ".hist";
        walk_str = record_file + ".walk";
        update_str = record_file + ".update";
        
        int open_hist = Init_save_hist( hist_str );  if(open_hist ==0) return 0;
//         Save_hist(hist_str);
        n_iteration++;
        n_loop++;
        int open_walk = Init_save_walk( walk_str); if(open_walk ==0) return 0;
        int open_update = Init_update( update_str); if(open_update ==0) return 0;
        cout << "Initialization of output files completed." << endl;
        
        cout << endl << "Wang_Landau initialization completed. Starting the density reconstruction." << endl << "Will record an update every " << n_update << endl << endl;
        
        bool accept;
        bool over_range = true;
        
        while(f-1  > crit_min){
                
                cout << "Iteration " << n_iteration << ". " ;
                cout << "Loop " << n_loop << " : " << setprecision(8) << "f =" << f <<  ", Qmin = " << Qmin << ", Qmax = " << Qmax << endl;
                while(score < crit_score_flat){
                        
                        if(n_walk_loop%n_update == 0){
                                Update(update_str);
                        }
                        
                        over_range=true;
                        while(over_range){
                                over_range=false;
                                for(int i=0; i<ndim;i++){
                                        xtest[i] = x[i] + sigma*normal(generator);
                                        if( xtest[i] > upp_bnd[i] || xtest[i] < low_bnd[i]){
                                                over_range=true;
                                                break;
                                        }
                                }                              
                        }
                        
                        Q=fcn(ndim,xtest, np,param_fcn);
                        Save_walk(walk_str, xtest, Q);
                        accept = Step(Q);
                        
                        if(accept) memcpy(x,xtest, ndim*sizeof(double));
                        
                }
                
                Measure_looptime();
                Save_hist(hist_str);
                End_loop();
                
                if(bound_var && bound_modified){
                        cout << "Range of Q changed during the first steps, starting over the Wang-landau algorithm with the new Q boundaries." << endl << endl; 
                        
                        double Qmns = Qmin;
                        double Qmxs = Qmax;
                        double nb = nbins; 
                        
                        Restart(param_file);
                }
                
        }
        
        free(x);
        free(xtest);
        
        Measure_totaltime();
        cout << endl << "Elapsed time is  "  << double(t_tot) /((double)CLOCKS_PER_SEC* 60.0) << "min."  << endl;
        
        return 0;      
        
}

