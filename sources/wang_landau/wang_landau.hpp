#pragma once

#include"base.hpp"
#include"GetPot.hpp"
#include"func.hpp"
#define _GLIBCXX_USE_CXX11_ABI 0


using namespace std;

struct Wang_Landau{ 
        
        // parameters of the Wang Landau
        double crit_min;
        double crit_score_flat;
        double lnf;
        double f;
        double pow_f;
        
        // parameters for variables update
        int ndim;
        double sigma;
        double sigma_min;
        double sigma_max;
        bool sigma_var;
        
        // parameters of the Q histogram
        bool bound_var; 
        bool bound_modified;
        double Qmin; 
        double Qmax;
        double Qmin_th;
        double Qmax_th;
        int nbins;
        double bin_size;
        
        // variables updated during the algorithm
        int *histogram;
        double *log_density;
        int bin_old;
        int bin_new;
        int average;
        double score;
        
        // counting 
        int n_iteration;
        int n_loop;
        int n_walk_loop;
        int n_walk_restart;
        int n_walk_tot;
        int n_update;
        
        // counting time        
        clock_t t_0;
        clock_t t_loop;
        clock_t t_tot;
        
        // initializations
        Wang_Landau():crit_min(0),crit_score_flat(0),nbins(0), bin_size(0),histogram(0),log_density(0),lnf(0),f(0),sigma(0), sigma_min(0), sigma_max(0), Qmin(0), Qmax(0), bin_old(0), bin_new(0),average(0),score(0), ndim(0), n_loop(0){};
        Wang_Landau(const char * param_file);
        ~Wang_Landau();
        int  Init(const char *param_file); // initialize the wang landau system
        int  Restart(const char * param_file); // if a restart is need (for example because the Q-boundaries changed) - keep the Q histogram parameters and the total count of points and time
        void Init(double Q); // initialize the algorithm with a first value of Q
        
        // one step
        void Update_score();
        void Update_sigma();
        int  Update_bounds(double Q);
        bool Step(double Q);
        
        // end of step
        void Measure_looptime();
        void Measure_totaltime();
        void Reset_hist();
        void Reset_logd();
        void Norm_logd();
        void End_loop();
        
        // display
        void Print_density();
        void Print_hist();
        
        // record        
        int  Init_save_hist(string filename);
        void Save_hist(string filename);
        int  Init_save_walk(string filename);
        void Save_walk(string filename, double *x, double Q);
        int  Init_update(string filename);
        void Update(string filename);
        void  Save_all_parameters(string filename);

        // whole process for a given fonction - for now fcn of type double(*ptrFcn)(int, double*, double*) 
        int  wang_landau(ptrFcn fcn, int n_dim, int np, double *param_fcn, double *low_bnd, double *upp_bnd, const char *param_file, string record_file);
        
                
};

