#include"func.hpp"

using namespace std;

double error_alert(int n, double *x, int np, double *sigma){
        cout << "Function name not recognized ... " << endl;
        return 0;
}

double gaussian(int n, double *x, int np, double *sigma){
        if(np != n){ cout << " Error, number of parameters and number of dimension are inconsistent in gaussian function! " << endl; return 0; }
        
        double Q=1;
        for(int i=0;i<np;i++){
                Q*= exp( - pow(x[i],2)/(2*pow(sigma[i],2)));
        }        
        return Q;

        
}

double gaussian_sum(int n, double *x, int np, double *sigma){
        if(np != 2*n){ cout << " Error, number of parameters and number of dimension are inconsistent in gaussian sum function! " << endl; return 0; }
        
        double Q=0;
        for(int i=0;i<n;i++){
                Q+= exp( - pow( (x[i]- sigma[i+n]),2)/(2*pow(sigma[i],2)));
        }
        Q /= n;

        return Q;
        
}

double cosinus(int n, double *x, int np, double *k){
        if(np != n){ cout << " Error, number of parameters and number of dimension are inconsistent in cosinus function! " << endl; return 0; }
        
        double Q=0;
        double argQ =0;
        for(int i=0;i<n;i++){
               argQ  += 2.0*M_PI/k[i] *x[i];
        }        
        Q=cos(argQ);        
        return Q;

}

double sigmoid(int n, double *x, int np, double *k){
        if(np != n){ cout << " Error, number of parameters and number of dimension are inconsistent in sigmoidal function! " << endl; return 0; }

        double Q=1;        
        for(int i=0;i<n;i++){
                Q*= 1/ (1+ exp( - k[i] * x[i]));
        }        
        return Q;
        
}

double sigmoid_sum(int n, double *x, int np, double *k){
        if(np != 2*n){ cout << " Error, number of parameters and number of dimension are inconsistent in sigmoid sum function! " << endl; return 0; }        
        
        double Q=0;
        for(int i=0;i<n;i++){
                Q+= 1/(1+ exp( - k[i] * (x[i] - k[i+n]) ));
        }
        
        
        return Q/n;
        
}

double fourier(int n, double *x, int np, double  *k){
        if(np != 2*n){ cout << " Error, number of parameters and number of dimension are inconsistent in fourier function! " << endl; return 0; }
        
        double Q=0;
        for(int i=0;i<n;i++){
               Q  += k[n+i] * cos(2.0*M_PI/k[i] *x[i]);
        }                
        return Q;
        
}

double complex_fcn(int n, double *x, int np, double *k){
        if(np != 4*n){ cout << " Error, number of parameters and number of dimension are inconsistent in complex function! " << endl; return 0; }

        double Q=0;
        Q=gaussian_sum(n, x, 2*n, k) + sigmoid_sum(n,x, 2*n, k+ 2*n);
        Q=Q/2; 

        return Q; 
        
}




