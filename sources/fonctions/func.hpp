#pragma once

#include "base.hpp"
#include <map>

// using namespace std;

typedef double(*ptrFcn)(int, double*, int, double*);
typedef map<const string, ptrFcn> FcnMap;

double error_alert(int n, double *x, int np, double *sigma);
double gaussian(int n, double *x, int np,  double *sigma);
double gaussian_sum(int n, double *x, int np, double *sigma);
double cosinus(int n, double *x, int np, double *k);
double sigmoid(int n, double *x, int np, double *k);
double sigmoid_sum(int n, double *x, int np, double *k);
double fourier(int n, double *x, int np, double*k);
double complex_fcn(int n, double *x, int np, double *k);


FcnMap create_FcnMap(){
        
        FcnMap fcnmap;
        fcnmap.emplace("error", &error_alert) ;
        fcnmap.emplace("gaussian", &gaussian) ;
        fcnmap.emplace("gaussian_s", &gaussian_sum) ;
        fcnmap.emplace("cosinus", &cosinus) ;
        fcnmap.emplace("sigmoid", &sigmoid) ;
        fcnmap.emplace("sigmoid_s", &sigmoid_sum) ;
        fcnmap.emplace("fourier", &fourier) ;
        fcnmap.emplace("complex", &complex_fcn) ;
        
        return fcnmap;
        
}


int gener_random(ptrFcn fcn, int ndim, int np, double *fcn_params, int n_pts, double * low_bnd, double *upp_bnd, string title){
        
        double *x = (double*)malloc(ndim*sizeof(double));
        double Q=0;
        
        ofstream random; 
        string random_str = title + ".random";
        if(fexists(random_str)){ cout << endl << "File already existing (.random) - stop here!" << endl; return 0; }
        random.open(random_str);
        
        for(int i=0; i<n_pts; i++){
                
                for(int d=0; d< ndim; d++){
                        x[d] = low_bnd[d] + (upp_bnd[d] - low_bnd[d]) * drand();
                        random << x[d] << ","; 
                }
                
                Q = fcn(ndim, x, np, fcn_params);
                random << Q << endl;
        }
        
        random.close();
        free(x);     
        
        return 0;
}

/*
int recursive_details(ptrFcn fcn, int ndim, double * fcn_params, double *x_tot, double *f_tot, int ntot, double precision){
        
        double *x = (double*)malloc(ndim*sizeof(double));
        double Qx = 0;
        double Qx1 =0;
        double Qx2 =0;
        double delta= 0;
        int nbegin = ntot;
        cout << " nbegin " << nbegin << endl;
        
        for(int i=0; i <nbegin; i++){
                for(int  j=0; j <nbegin ; j++){
                        
                        cout << "x : " ;
                        for(int d=0; d<ndim;d++){
                                x[d] = ( x_tot[i*ndim + d] + x_tot[j*ndim +d] ) /2;
                                cout << "i " << i << "  xi " << x_tot[i*ndim + d] << " j " << "  xj " << x_tot[j*ndim + d] << j << " d " << d << " x " << x[d] << "  " ;
                        }                        
                        Qx = fcn(ndim, x, fcn_params);
                        Qx1 = fcn(ndim, x_tot + i*ndim, fcn_params);
                        Qx2 = fcn(ndim, x_tot + j*ndim, fcn_params);
                        
                        cout << "Qx " << Qx << " Qx1 " << Qx1 << " Qx2 " << Qx2 ;
                        
                        delta = 2*Qx / (Qx1+Qx2);
                        cout << " delta " << delta << endl;
                        
                        if(delta > precision){
                                ntot ++; cout << "first iteration, ntot " << ntot << endl;
                                myrealloc( x_tot, ntot*ndim*sizeof(double), (ntot-1)*ndim*sizeof(double) );
                                myrealloc( f_tot, ntot*sizeof(double), (ntot+1)*sizeof(double));

                                for(int d=0; d< ndim; d++){
                                        x_tot[ntot*ndim + d] = x[d];
                                }
                                f_tot[ntot-1] = Qx;
                        }
                        
                }
        }
        
        free(x);
        
//         if(ntot != nbegin){ recursive_details(fcn, ndim, fcn_params, x_tot, f_tot, ntot, precision);}
        cout << "End of the recursive !" << endl;
//         return 0;
        return ntot;
        
        
        
}

int gener_details(ptrFcn fcn, int ndim, double * fcn_params, double * low_bnd, double *upp_bnd, double precision){
        
        double nini = pow(10,ndim);
        double *x_tot = (double*)malloc(nini*ndim*sizeof(double));
        double *f_tot = (double*)malloc(nini*sizeof(double));
        
        for(int i=0; i<nini; i++){
                
                for(int d=0; d< ndim; d++){
                        x_tot[i*ndim + d] = low_bnd[d] + (upp_bnd[d] - low_bnd[d]) * drand();
                        cout << x_tot[i*ndim + d] << "  ";
                }
                f_tot[i]  = fcn(ndim, x_tot + i*ndim, fcn_params);
                cout << f_tot[i] << endl;
                
        }
        
//         int ntot=nini;
        int ntot = recursive_details(fcn, ndim, fcn_params, x_tot, f_tot, nini, precision);      
        
        free(x_tot);
        free(f_tot);
        
        return ntot;
}


*/



