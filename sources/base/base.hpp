#pragma once

#define _GLIBCXX_USE_CXX11_ABI 0


#include <string.h>
#include <cstdio>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <time.h>
#include <cstdlib>
#include <random>
#include <ctime>
#include <stdarg.h>

using namespace std;

void readdata(string filename, double *datatab);
bool fexists(const std::string& filename);

mt19937_64 initiate_random_generator();

void print(double *mat, int nlin, int ncol );
void print(int *mat, int nlin, int ncol );
void decal(double *data, int nelem, int nadd);
void decal(int *data, int nelem, int nadd);
void* realloc_s(void** ptr, size_t size);

template<typename T> void myrealloc(T ptr, size_t size, size_t old_size){
        
        T ptr_temp = (T)malloc(size);
        if(ptr_temp ==NULL){cout << "memory error "<<  endl;}
        
        memcpy(ptr_temp, ptr, old_size); 
        ptr = ptr_temp;       
        free(ptr_temp);
        
        
}
// void realloc_s(int **ptr, int nelem);

string gener_filename(int nspins, int ngrad, int nsites, double temperature);

inline mt19937_64 test(){
        
        mt19937_64 generator =initiate_random_generator();
        return generator;
        
}

double gener(mt19937_64 &generator, normal_distribution<double> normal);

// generates a random double number d, uniformly distributed beween 0 and 1 (0 <= d < 1) 
inline double drand(){
        return rand()/(double)RAND_MAX;
}
// generates a random integer number i, uniformly distributed beween 0 and n (0 <= i < n)
inline int irand(int n){
        return rand()%n;
}



