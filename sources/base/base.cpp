#include"base.hpp"

using namespace std;


// Function to see if there is a file name like this
bool fexists(const std::string& filename) {
        ifstream ifile(filename.c_str());
        return (bool)ifile;
}

// Function to put data from a file to a tab
void readdata(string filename, double *datatab){
        ifstream data(filename);
        if(data){
                int i=0;
                while(data >> datatab[i])
                        i++;                
        }        
        else{
                cout << "Error, could not open input data file" << endl;
        }
}

mt19937_64 initiate_random_generator(){
        unsigned int mersenneSeed = irand(100000);
        mt19937_64 generator; // it doesn't matter if I use the 64 or the std::mt19937
        generator.seed(mersenneSeed);
        return generator;
}

double gener(mt19937_64 &generator, normal_distribution<double> normal){
        double gauss = normal(generator);
        
        for(int i=0; i<3; i++){
                gauss = normal(generator);
                cout << gauss << endl;
//                 cout << "in function " << endl << generator << endl;
        }
        return gauss;
}

// Function to display a matrix of size nlin*ncol
void print(double *mat, int nlin, int ncol ){
        cout << endl;
        for(int i=0; i<nlin; i++){
                for (int j=0; j<ncol; j++){
                        cout <<  setprecision(4) << mat[j*nlin+i] << "\t" ;
                }
                
                cout << endl;
        }
        cout << endl;
}

void print(int *mat, int nlin, int ncol ){
        cout << endl;
        for(int i=0; i<nlin; i++){
                for (int j=0; j<ncol; j++){
                        cout <<  mat[j*nlin+i] << "\t" ;
                }
                
                cout << endl;
        }
        cout << endl;
}

void* realloc_s(void **ptr, size_t size){
        void *ptr_realloc = realloc(*ptr, size);
        if(ptr_realloc  ==NULL){cout << "memory error "<<  endl;};
        *ptr=ptr_realloc;
        return ptr_realloc;
}

void decal(double *data, int nelem, int nadd){
        for(int i=nelem; i>0; i--){
                data[i+nadd-1] = data[i-1];
        }
        for(int i=0; i< nadd; i++){
                data[i]=0;
        }        
}


void decal(int *data, int nelem, int nadd){
        for(int i=nelem; i>0; i--){
                data[i+nadd-1] = data[i-1];
        }
        for(int i=0; i< nadd; i++){
                data[i]=0;
        }
}


string gener_filename(int nspins, int ngrad, int nsites, double temperature){
        string filename;
//         ostringstream T;
//         
//         filename = to_string(nspins) + "s" + to_string(ngrad) + "g_sit" + to_string(nsites); 
//         T << setprecision(3) << temperature;
//         filename += "_T" +T.str();
//         
        return filename;
}






